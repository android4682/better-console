export enum LogLevel {
  silent,
  info,
  warn,
  error,
  debug,
  dev
}

export class BetterConsole implements Console
{
  public static TerminalColors = {
    brighter: "\x1b[1m",
    darker: "\x1b[2m",
    reset: "\x1b[0m",
    white: "\x1b[37m",
    lightWhite: "\x1b[0m\x1b[1m\x1b[37m",
    darkWhite: "\x1b[0m\x1b[2m\x1b[37m",
    red: "\x1b[31m",
    lightRed: "\x1b[0m\x1b[1m\x1b[31m",
    darkRed: "\x1b[0m\x1b[2m\x1b[31m",
    yellow: "\x1b[33m",
    lightYellow: "\x1b[0m\x1b[1m\x1b[33m",
    darkYellow: "\x1b[0m\x1b[2m\x1b[33m",
    cyan: "\x1b[36m",
    lightCyan: "\x1b[0m\x1b[1m\x1b[36m",
    darkCyan: "\x1b[0m\x1b[2m\x1b[36m",
    blue: "\x1b[34m",
    lightBlue: "\x1b[0m\x1b[1m\x1b[34m",
    darkBlue: "\x1b[0m\x1b[2m\x1b[34m",
    green: "\x1b[32m",
    lightGreen: "\x1b[0m\x1b[1m\x1b[32m",
    darkGreen: "\x1b[0m\x1b[2m\x1b[32m",
    purple: "\x1b[35m",
    lightPurple: "\x1b[0m\x1b[1m\x1b[35m",
    darkPurple: "\x1b[0m\x1b[2m\x1b[35m",
    gray: "\x1b[2m\x1b[37m",
    lightGray: "\x1b[0m\x1b[1m\x1b[37m",
    darkGray: "\x1b[0m\x1b[2m\x1b[37m"
  }
  
  public Console: console.ConsoleConstructor;
  protected internal_logLevel: LogLevel

  public static Create(logLevelOverride?: LogLevel): BetterConsole
  {
    if (logLevelOverride !== undefined) {
      return new this(logLevelOverride)
    } else {
      let logLevel: LogLevel | undefined = undefined
      
      // Trying process arguments first
      let logLevelIndex = process.argv.indexOf('--log-level')
      if (logLevelIndex !== -1) {
        let logLevelArg = parseInt(process.argv[logLevelIndex+1])
        if (! Number.isNaN(logLevelArg)) logLevel = <LogLevel> logLevelArg
      }
      
      // No LogLevel argument found, tyring env
      if (logLevel === undefined) {
        let logLevelArg = parseInt(process.env['loglevel'] || 'NaN')
        if (! Number.isNaN(logLevelArg)) logLevel = <LogLevel> logLevelArg
      }

      // If no LogLevel is found we will be using the default value
      return new this(logLevel)
    }
  }

  public constructor(logLevel: LogLevel = LogLevel.error)
  {
    this.Console = console.Console;
    this.internal_logLevel = logLevel
  }

  public get logLevel(): LogLevel
  {
    return this.internal_logLevel
  }

  public set logLevel(value: LogLevel)
  {
    this.internal_logLevel = value
  }

  protected get Static(): typeof BetterConsole
  {
    return (this.constructor as typeof BetterConsole)
  }

  public shouldLog(level: LogLevel): boolean
  {
    return (this.internal_logLevel >= level)
  }

  protected dataToString(data: any[], preColor: string = '', postColor: string = this.Static.TerminalColors.reset): string
  {
    for (let i = 0; i < data.length; i++) {
      const value = data[i];
      if (typeof value === 'object') data[i] = JSON.stringify(value)
    }
    let nData: string = data.join(" ")
    return `${preColor}${nData}${postColor}`
  }

  public profile(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.profile(label)
  }
  
  public profileEnd(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.profileEnd(label)
  }
  
  public assert(condition?: boolean | undefined, ...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.assert(condition, ...data);
  }

  public clear(): void
  {
    return console.clear();
  }

  public count(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.count(label);
  }
  
  public countReset(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.countReset(label);
  }
  
  public debug(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.debug(this.dataToString(data, this.Static.TerminalColors.purple));
  }
  
  public dir(item?: any, options?: any, logLevel: LogLevel = LogLevel.debug): void
  {
    if (! this.shouldLog(logLevel)) return
    return console.dir(item, options);
  }
  
  public dirxml(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.dirxml(...data);
  }
  
  public error(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.error)) return
    return console.error(this.dataToString(data, this.Static.TerminalColors.red));
  }
  
  public group(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.group(...data);
  }
  
  public groupCollapsed(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.groupCollapsed(...data);
  }
  
  public groupEnd(): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.groupEnd();
  }
  
  public info(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.info)) return
    return console.info(this.dataToString(data, this.Static.TerminalColors.lightBlue));
  }
  
  public log(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.info)) return
    return console.log(...data);
  }
  
  public success(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.info)) return
    return console.log(this.dataToString(data, this.Static.TerminalColors.green));
  }
  
  public dev(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.dev)) return
    return console.debug(this.dataToString(data, this.Static.TerminalColors.darkPurple));
  }
  
  public table(tabularData?: any, properties?: string[] | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.table(tabularData, properties);
  }
  
  public time(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.time(label);
  }
  
  public timeEnd(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.timeEnd(label);
  }
  
  public timeLog(label?: string | undefined, ...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.timeLog(label, ...data);
  }
  
  public timeStamp(label?: string | undefined): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.timeStamp(label);
  }
  
  public trace(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.debug)) return
    return console.trace(...data);
  }
  
  public warn(...data: any[]): void
  {
    if (! this.shouldLog(LogLevel.warn)) return
    return console.warn(this.dataToString(data, this.Static.TerminalColors.yellow));
  }
}

export function getMessageAndStack(e: any): [string, object | undefined]
{
  let msg: string = e
  let stacktrace: object | undefined
  if (typeof e === "object") {
    if (!!e.message) {
      msg = e.message
      stacktrace = e.stack
    } else if (!!e.sqlMessage) {
      msg = e.sqlMessage
      stacktrace = e
    } else stacktrace = e
  }
  return [msg, stacktrace]
}

export function errorLog(e: any, logger: BetterConsole): void
{
  let [msg, stacktrace] = getMessageAndStack(e)
  logger.error("Error: " + msg)
  logger.error(stacktrace)
}
